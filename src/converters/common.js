import Logger from '../tools/logger';

var log = new Logger('common');

function convertDuration(seconds) {
    let product;
    let date = new Date(null);
    date.setSeconds(seconds);

    product = date.toISOString().substr(11, 8);
    if (date.getDate() > 1) {
        product = (date.getDate() - 1) + '.' + product;
    }
    
    return product;
}

function convertDate(date) {
    var date = new Date(date.split(': ').join(':'));
    return date.toISOString().substr(0, 19) + '+02:00';
}

function cleanup(aircrafts, batteries, events) {
    fixFlights(events.flights, aircrafts, batteries);
    fixCharges(events.charges, aircrafts, batteries);
    cleanObjects(aircrafts);
    cleanObjects(batteries);
}

function fixFlights(flights, aircrafts, batteries) {
    let missingBattery = 0;
    let missingModel = 0;
    flights.forEach(element => {
        for(let i = 0; i < element.Batteries.length; ++i) {
            let battery = getItem(batteries, element.Batteries[i]);
            element.Batteries[i] = battery ? battery.Name : 'Unlisted pack';
            if (!battery) {
                missingBattery++;
            }
        }
        let model = getItem(aircrafts, element.ModelName);
        if (model) {
            element.ModelName = model.Name;
        }
        else {
            missingModel++;
        }

    });
    if (missingBattery) {
        log.warn('battery missing for ' + missingBattery + ' flights');
    }
    if (missingModel) {
        log.warn('model missing for ' + missingModel + ' flights');
    }
}

function fixCharges(charges, aircrafts, batteries) {
    let missingBattery = 0;
    let missingModel = 0;
    charges.forEach(element => {
        for(let i = 0; i < element.Batteries.length; ++i) {
            let battery = getItem(batteries, element.Batteries[i]);
            if (battery) {
                element.Batteries[i] = battery.Name;
            } else {
                missingBattery++;
                element.Batteries.splice(i--, 1);
            }
        }
        if (element.FlownModel) {
            let model = getItem(aircrafts, element.FlownModel);
            if (model) {
                element.FlownModel = model.Name;
                if (!element.Details) {
                    element.Details = 'Flown ' + element.FlownModel;
                }
            }
            else {
                missingModel++;
            }
        }
    });
    if (missingBattery) {
        log.warn('battery missing for ' + missingBattery + ' charges, removing.');
        for (var i = 0; i < charges.length; ++i) {
            if (!charges[i].Batteries.length) {
                charges.splice(i--, 1);
            }
        }
    }
    if (missingModel) {
        log.warn('model missing for ' + missingModel + ' charges');
    }
}

function getItem(list, id) {
    return list.find(element => {
        return element._id === id;
    });
}

function cleanObjects(list) {
    list.forEach(element => {
        delete element._id;
    });
}

export {
    convertDate,
    convertDuration,
    cleanup
};