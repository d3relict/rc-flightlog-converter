import Logger from '../tools/logger';
import AIRCRAFT from './const/aircraft.json';
import object from '../tools/object';

import { convertDate } from './common';
import { convertDuration } from './common';

const log = new Logger('aircrafts');

const translation = [
    { 'from' : 'NAME', 'to' : 'Name'},
    { 'from' : 'BATTERY_CELLS', 'to' : 'CellsPerPack'},
    { 'from' : 'BATTERY_CELLS', 'to' : 'MaxCellsPerPack'},
    { 'from' : 'ROW_REQUIRED_NUMBER_OF_FLIGHT_BATTERIES', 'to' : 'PackCount'},
    { 'from' : 'ROW_COST', 'to' : 'Cost'},
    { 'from' : 'ROW_NOTE', 'to' : 'Description'},
    { 'from' : 'NUMBER_OF_FLIGHTS', 'to' : 'FlightCount'}
];

function convert(aircrafts) {
    var product = [];

    log.debug('converting ' + aircrafts.length + ' aircrafts');


    aircrafts.pop();
    aircrafts.forEach(element => {
        let aircraft = object.clone(AIRCRAFT);
        object.translate(element, aircraft, translation);

        aircraft.FlightDuration = convertDuration(element.DEFAULT_FLIGHTTIME);
        aircraft.TotalFlightTime = convertDuration(element.USED_IN_SEC);
        aircraft.PurchaseDate = convertDate(element.PURECHASEDATE);
        aircraft._id = element._id;

        product.push(aircraft);
    });

    return product;
}

function process(aircrafts, flights) {
    aircrafts.forEach(aircraft => {
        let related = flights.filter(element => {
            return element.ModelName === aircraft.Name;
        });
        if (aircraft.FlightCount != related.length) {
            log.warn('aircraft flight count mismatch, resetting');
            aircraft.FlightCount = related.length;
        }
        aircraft.LastFlightDate = related[0].Date;
        aircraft.CrashCount = 0;
        related.map(flight => {
            if (flight.Reason === 1) {
                if (!aircraft.CrashCount) {
                    aircraft.LastCrashDate = flight.Date;
                }
                aircraft.CrashCount++;
            }
        });
        if (aircraft.LastCrashDate == aircraft.LastFlightDate) {
            aircraft.State = 1;
        }
    });
}

export default { convert, process };