import Logger from '../tools/logger';
import object from '../tools/object';
import FLIGHT from './const/flight.json';
import CHARGE from './const/charge.json';

import { convertDate } from './common';
import { convertDuration } from './common';

const log = new Logger('flights');

function convert(events) {
    log.debug('converting ' + events.length + ' events');
    let flights = [];
    let charges = [];

    events.forEach(element => {
        switch(element.TYP) {
            case 'FLIGHT':
                flights.push(convertFlight(element));
                //don't break, flights are also charge events
            case 'CHARGE':
            case 'DISCHARGE':
                charges.push(convertCharge(element));
                break;
        }
    });

    log.debug('   ' + flights.length + ' flights,');
    log.debug('   ' + charges.length + ' charges');

    return {
        flights,
        charges
    }
}

function convertCharge(element) {
    let charge = object.clone(CHARGE);
    
    charge.Date = convertDate(element.CREATIONDATE);
    charge.mAH = element.USED_CAPACITY_IN_MAH;
    charge.Batteries.push(element.ID_BATTERY);

    if (element.ID_AIRCRAFT != -1) {
        charge.FlownModel = element.ID_AIRCRAFT;
    }

    switch (element.TYP) {
        case 'CHARGE':
            charge.State = (element.STORE === 'STORE') ? 1 : 2;
            break;
        case 'DISCHARGE':
        case 'FLIGHT':
            charge.State = (element.ROW_BATTERY_CAPACITY_IN_PER_THOUSAND < 400) ? 0 : 1;
            break;
    }
    
    return charge;
}

function convertFlight(element) {
    let flight = object.clone(FLIGHT);

    flight.Duration = convertDuration(element.DUARATION_IN_SEC);
    flight.Date = convertDate(element.CREATIONDATE);
    flight.Batteries.push(element.ID_BATTERY);
    flight.ModelName = element.ID_AIRCRAFT;
    flight.Details = element.COMMENT;

    switch (element.SPECIALS) {
        case '_CRASHED_':
            flight.Reason = 1;
            flight.FlightResult = 1;
            flight.CrashReason = 0;
            break;
        case '_DEEP_DISCHARD_':
            break;
        case '_ROUGH_LANDING_':
            break;
    }

    return flight;
}

export default { convert };