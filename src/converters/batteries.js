import Logger from '../tools/logger';
import BATTERY from './const/battery.json';
import object from '../tools/object';

import { convertDate } from './common';

const log = new Logger('batteries');

const translation = [
    { 'from' : 'CAPACITY', 'to' : 'Capacity'},
    { 'from' : 'BATTERY_CELLS', 'to' : 'CellCount'},
    { 'from' : 'number_of_fights', 'to' : 'ChargeCount'},
    { 'from' : 'ROW_NOTE', 'to' : 'Notes'},
    { 'from' : 'ROW_C_FACTOR_CHARGE', 'to' : 'ChargeRate'},
    { 'from' : 'ROW_C_FACTOR_DISCHARGE', 'to' : 'DischargeRate'}
];

function getName(element) {
    return '#' + element.NUMBER + ' ' + element.NAME;
}

function convert(batteries) {
    let product = [];

    log.debug('converting ' + batteries.length + ' batteries');

    batteries.pop();
    batteries.forEach(element => {
        let battery = object.clone(BATTERY);
        let charge = +element.ROW_BATTERY_FILL_LEVEL_IN_PERCENT;

        object.translate(element, battery, translation);

        battery.Name = getName(element);
        battery.PurchaseDate = convertDate(element.PURECHASEDATE);
        battery.ChargeState = (charge >= 60) ? 2 : ((charge >= 40) ? 1 : 0);
        battery.Cost = element.ROW_COST / 100;
        
        //temp data for pass 2
        battery._id = element._id;

        product.push(battery);
    });

    return product;
}

function process(batteries, charges) {
    batteries.forEach(battery => {
        let chargeCount = 0;
        let related = charges.filter(charge => {
            return charge.Batteries.indexOf(battery.Name) >= 0;
        });
        
        battery.DateLastUsed = related[0].Date;
        related.map(charge => {
            if (charge.State === 2) {
                if (!chargeCount) {
                    battery.DateLastCharged = charge.Date;
                }
                chargeCount++;
            }
        });
        if (chargeCount != battery.ChargeCount) {
            log.warn(battery.Name + ' charge count mismatch (' + chargeCount + ' <> ' + battery.ChargeCount + '), setting ' + chargeCount);
        }
    });
}

export default { convert, process };