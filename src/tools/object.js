function clone(object) {
    return JSON.parse(JSON.stringify(object));
}

function translate(source, target, list) {
    list.forEach(element => {
        if (source.hasOwnProperty(element.from) && target.hasOwnProperty(element.to)) {
            if (typeof target[element.to] === 'number') {
                target[element.to] = +source[element.from];
            }
            else {
                target[element.to] = source[element.from];
            }
        }
        else {
            throw new Error('Object key mismatch. ' + JSON.stringify(element));
        }
    });
}

export default { clone, translate };