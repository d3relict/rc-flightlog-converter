const LEVEL_DEBUG = 4;
const LEVEL_INFO = 3;
const LEVEL_WARN = 2;
const LEVEL_ERROR = 1;

var paddingCount = 0;
var EMOJIS = ['', '⛔', '⚠️', 'ℹ️', '⚙️'];

function padding(length) {
    let product = ' ';
    while(product.length < length) {
        product += product;
    }
    return product.substr(0, length);
}

function padLabel(label) {
    return '[' + label + ']' + padding(paddingCount - label.length);
}

class Logger {
    constructor (label) {
        paddingCount = Math.max(paddingCount, label.length);

        this.debug = this._log.bind(this, LEVEL_DEBUG, label);
        this.info = this._log.bind(this, LEVEL_INFO, label);
        this.warn = this._log.bind(this, LEVEL_WARN, label);
        this.error = this._log.bind(this, LEVEL_ERROR, label);
    }

    _log(level, label, message) {
        console.log(
            new Date().toISOString().substr(11,12) + '  ' + EMOJIS[level] + '  ' + padLabel(label) + '  ' + message
        );
    }
}

export default Logger;