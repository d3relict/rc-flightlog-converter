import Logger from './tools/logger';
import aircraftConverter from './converters/aircrafts';
import batteryConverter from './converters/batteries';
import eventConverter from './converters/events';

import { cleanup } from './converters/common';

const log = new Logger('converter');

function convert(data) {
    log.info('---- pass 1 ----');
    let aircrafts = aircraftConverter.convert(data.aircrafts);
    let batteries = batteryConverter.convert(data.batteries);
    let events = eventConverter.convert(data.flights);

    log.info('---- pass 2 ----');
    cleanup(aircrafts, batteries, events);
    
    log.info('---- pass 3 ----');
    aircraftConverter.process(aircrafts, events.flights);        
    batteryConverter.process(batteries, events.charges);        

    log.info('results:');
    log.info('   models: ' + aircrafts.length);
    log.info('   batteries: ' + batteries.length);
    log.info('   flights: ' + events.flights.length);
    log.info('   charges: ' + events.charges.length);

    return {
        models: aircrafts,
        batteries: batteries,
        charges: events.charges,
        flights: events.flights
    }
}

export default { convert };