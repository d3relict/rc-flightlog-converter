import Logger from './tools/logger';
import reader from './reader';
import writer from './writer';
import converter from './converter';

const log = new Logger('index');

const prefix = process.argv[2];

reader.read(prefix).then((data) => {
    writer.write(converter.convert(data));
});