import fs from 'fs';
import Logger from './tools/logger';

const log = new Logger('writer');

function write(data) {
    let writeCount = 0;

    function check() {
        writeCount++;
        if (writeCount == 4) {
            log.info('file write complete');
        }
    }

    fs.writeFile("./data/Batteries.json", JSON.stringify(data.batteries), function(err) {
        log.debug('writing Batteries.json finished');
        check();
    }); 
    fs.writeFile("./data/Charges.json", JSON.stringify(data.charges), function(err) {
        log.debug('writing Charges.json finished');
        check();
    }); 
    fs.writeFile("./data/Flights.json", JSON.stringify(data.flights), function(err) {
        log.debug('writing Flights.json finished');
        check();
    }); 
    fs.writeFile("./data/Models.json", JSON.stringify(data.models), function(err) {
        log.debug('writing Models.json finished');
        check();
    }); 
}

export default { write }
