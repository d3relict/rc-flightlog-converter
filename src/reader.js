import csv from 'csv-parser';
import fs from 'fs';
import Logger from './tools/logger';

const log = new Logger('reader');

function readCSV(filename) {
    return new Promise((resolve, reject) => {
        let product = [];
        fs.createReadStream('./data/' + filename)
            .pipe(csv())
            .on('data', (data) => {
                product.push(data);
            })
            .on('finish', () => {
                resolve(product);
            });
    });
}

function read(prefix) {
    return new Promise((resolve, reject) => {
        let product = {};

        let check = () => {
            if (product.aircrafts && product.batteries && product.flights) {
                log.info('all data loaded');
                resolve(product);
            }
        };

        readCSV(prefix + '_BackupAircraft.txt').then((list) => {
            product.aircrafts = list;
            log.debug('aircrafts loaded: ' + list.length);
            check();
        });

        readCSV(prefix + '_BackupBattery.txt').then((list) => {
            product.batteries = list;
            log.debug('batteries loaded: ' + list.length);
            check();
        });

        readCSV(prefix + '_BackupFlights.txt').then((list) => {
            product.flights = list;
            log.debug('flights loaded: ' + list.length);
            check();
        });
    });
}

export default {read};